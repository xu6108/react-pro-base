import { Button, Card, Form, Input, message } from 'antd'
import React, { useEffect } from 'react'
import styles from "./index.module.less";
import { reqHospitalAdd, type reqHospitalAddState, reqHospitalDataById, reqHospitalUpdData } from '@/api/user/indexHos';
import { useNavigate, useParams } from "react-router-dom";

export default function AddHospital() {

    const navigate = useNavigate();
    const [form] = Form.useForm();
    const { id } = useParams();

    useEffect(() => {
        const getHospitalDataByid = async () => {
            if (!id) return;
            const result = await reqHospitalDataById(+id);
            form.setFieldsValue(result);
        }
        getHospitalDataByid();
    }, [])



    const [messageApi, contextHolder] = message.useMessage();

    const finish = async (values: reqHospitalAddState) => {
        if (id) {
            await reqHospitalUpdData({ ...values, id: +id });
            messageApi.open({
                type: 'success',
                content: '医院信息修改成功！',
            });
            navigate("/syt/hospital/hospitalSet");
        } else {
            await reqHospitalAdd(values);
            messageApi.open({
                type: 'success',
                content: '医院信息添加成功！',
            });
            navigate("/syt/hospital/hospitalSet");
        }
    }

    return (
        <Card>
            {contextHolder}
            <Form form={form} labelCol={{ span: 2 }} wrapperCol={{ span: 20 }} onFinish={finish}>
                <Form.Item label="医院名称" name="hosname"
                    rules={[{ required: true, message: "该内容必填" }]}>
                    <Input />
                </Form.Item>
                <Form.Item label="医院编号" name="hoscode"
                    rules={[{ required: true, message: "该内容必填" }]}>
                    <Input />
                </Form.Item>
                <Form.Item label="api基础路径" name="apiUrl"
                    rules={[{ required: true, message: "该内容必填" }]}>
                    <Input />
                </Form.Item>
                <Form.Item label="联系人姓名" name="contactsName"
                    rules={[{ required: true, message: "该内容必填" }]}>
                    <Input />
                </Form.Item>
                <Form.Item label="联系人手机" name="contactsPhone"
                    rules={[{ required: true, message: "该内容必填" },
                    { pattern: /^[0-9]{11}$/g, message: "手机号必须为11位" }]}>
                    <Input />
                </Form.Item>
                <Form.Item >
                    <Button
                        type='primary'
                        className={styles.mr}
                        style={{ marginLeft: "100px" }}
                        htmlType='submit'
                    >{id ? "修改" : "保存"}</Button>
                    <Button onClick={() => {
                        navigate("/syt/hospital/hospitalSet");
                        }}>返回</Button>
                </Form.Item>
            </Form>
        </Card>
    )
}