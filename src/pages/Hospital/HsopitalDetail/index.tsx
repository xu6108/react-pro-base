import { Card, Descriptions } from 'antd';
import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom';
import { reqHosListById ,type reqHosListByIdType} from '@/api/HospitalList';

export default function HospitalDetail() {
  const [hospitalDetailData,sethospitalDetailData]=useState<reqHosListByIdType>();
  const {state} = useLocation();
  const {id} = (state as any);

  useEffect(()=>{
    const getReqHosListById = async()=>{
        if(!id)return;
        const result = await reqHosListById(id);
        console.log(result);
        sethospitalDetailData(result);
    }
    getReqHosListById();
  },[])

  return (
    <Card>
      <Descriptions title="基本信息" bordered column={2}>
        <Descriptions.Item label="医院名称">{hospitalDetailData?.hospital.hosname}</Descriptions.Item>
        <Descriptions.Item label="医院logo">
        {hospitalDetailData?.hospital.logoData && <img src={"data:image/jpge;base64,"+hospitalDetailData?.hospital.logoData} width="100px"></img>}
        </Descriptions.Item>
        <Descriptions.Item label="医院编码">{hospitalDetailData?.hospital.hoscode}</Descriptions.Item>
        <Descriptions.Item label="医院地址">{hospitalDetailData?.hospital.param?.fullAddress}</Descriptions.Item>
        <Descriptions.Item label="坐车路线" span={2}>
        {hospitalDetailData?.hospital.route}
        </Descriptions.Item>
        <Descriptions.Item label="医院简介" span={2}>
        {hospitalDetailData?.hospital.intro}
        </Descriptions.Item>
      </Descriptions>
      <Descriptions
        title="预约规则信息"
        bordered
        column={2}
        style={{ marginTop: "40px" }}
      >
        <Descriptions.Item label="预约周期">{hospitalDetailData?.bookingRule.cycle}</Descriptions.Item>
        <Descriptions.Item label="放号时间">{hospitalDetailData?.bookingRule.releaseTime}</Descriptions.Item>
        <Descriptions.Item label="停接时间">{hospitalDetailData?.bookingRule.stopTime}</Descriptions.Item>
        <Descriptions.Item label="退号时间">{hospitalDetailData?.bookingRule.quitTime}</Descriptions.Item>
        <Descriptions.Item label="预约规则" span={2}>
        {hospitalDetailData?.bookingRule.rule.map((item,index)=>{
            return (
                <div key={index}>{item}</div>
            )
        })}
        </Descriptions.Item>
      </Descriptions>
    </Card>
  )
}
