import React, { useState } from 'react'
import { Form, Input ,Button,Table,Card, message} from 'antd';
import {SearchOutlined,EditOutlined,CloseOutlined} from "@ant-design/icons";
import styles  from "./index.module.less";
import { reqHospital, reqHospitalDeleteByid,reqHospitalBatchDeleteByidList} from '@/api/user/indexHos';
import { useEffect } from 'react';

import type {hosReqDataType,hosReqListItemType} from '@/api/user/indexHos';
import { useNavigate } from 'react-router-dom';
import { Key } from 'antd/es/table/interface';

export default function HospitalList() { 
  //数据1:页码的当前页
  const [current, setCurrent] = useState<number>(1);
  //数据2:数据总数
  const [total, setTotal] = useState<number>(0);
  //当前显示条数
  const [pageSize, setpageSize] = useState<number>(3);
  //loading
  const [isLoading,setIsloading] = useState(false);

  const [idList,setIdList] = useState<Key[]>([]);
  //表格数据
  const [hospitalList, sethospitalList] = useState<hosReqDataType>([]);

  const [messageApi, contextHolder] = message.useMessage();

  const [form] = Form.useForm();//获取表单数据

  const navigate = useNavigate();

  // interface searchStateType {
  //   hoscode?: string
  //   hosname?: string
  // }


  //第二种方法
  const getHospitalList = async()=>{
    setIsloading(true);
    const {hosname,hoscode} = form.getFieldsValue();
    const result = await reqHospital({
        page:current,
        limit:pageSize,
        hosname,
        hoscode
      });
      setCurrent(result.current);
      setTotal(result.total);
      sethospitalList(result.records);
      setIsloading(false);
  }

  // const rowSelection = {
  //   onchange(a:any){
  //     console.log(a)
  //   }
  // }

  // const getHospitalList = async({hosname,hoscode}:searchStateType)=>{
  //     const result = await reqHospital({
  //       page:current,
  //       limit:pageSize,
  //       hosname,
  //       hoscode
  //     });
  //     setCurrent(result.current);
  //     setTotal(result.total);
  //     sethospitalList(result.records);
  // }
  
  //初始化页面渲染数据
  useEffect(()=>{
    //getHospitalList({});
    getHospitalList();
  },[current,pageSize])

  const searchFinish = ()=>{
    // getHospitalList(values);
    getHospitalList();
  }

  const updateHos =(id:number)=>{
    return ()=>{
      navigate(`/syt/hospital/hospitalSet/update/${id}`);
    }
  }

  const deleteHos = (id:number)=>{
    return async()=>{
      await reqHospitalDeleteByid(id);

      messageApi.open({
          type: 'success',
          content: '医院信息删除成功！',
      });

      getHospitalList();
    }
  }

  const batchDelete=async()=>{
    await reqHospitalBatchDeleteByidList(idList as number[]);
    
    messageApi.open({
        type: 'success',
        content: '医院信息批量删除成功！',
    });

    getHospitalList();
  }
  

  const rowSelection = {
    //onChange函数接受的第一个参数是选中值id组成的数组 第二个参数是选中值数据组成的数组
    async onChange(idList: Key[]) {
      setIdList(idList);
    },
  };

  const columns = [
    {
      title:"序号",
      fixed:"left" as "left",
      render(_:any,__:any,index:number){
        return index + 1
      }
    },
    {
      title: "医院名称",
      dataIndex: "hosname",
    },
    {
      title: "医院编号",
      dataIndex: "hoscode",
    },
    {
      title: "api基础路径",
      dataIndex: "apiUrl",
    },
    {
      title: "签名",
      dataIndex: "signKey",
    },
    {
      title: "联系人姓名",
      dataIndex: "contactsName",
    },
    {
      title: "联系人手机",
      dataIndex: "contactsPhone",
    },
    {
      title:"操作列",
      width:"100",
      fixed:"right" as "right",
      render(row:hosReqListItemType){
        return(
          <div>
            <Button icon={<EditOutlined />}  type="primary"  className={styles.mr}
            onClick={updateHos(row.id)}></Button>
            <Button icon={<CloseOutlined />} type="primary" danger onClick={deleteHos(row.id)}></Button>
          </div>
        )
      }
    }
  ];

  return (
    <Card>
      {contextHolder}
       <Form  form={form} layout="inline" className={styles.mb} onFinish={searchFinish}>
      <Form.Item name="hosname">
        <Input placeholder="医院名称" />
      </Form.Item>
      <Form.Item name="hoscode">
        <Input placeholder="医院编号"/>
      </Form.Item>
      <Form.Item>
        <Button type="primary" icon={<SearchOutlined/>} htmlType="submit">查询</Button>
      </Form.Item>
      <Form.Item>
        <Button onClick={()=>{
          form.resetFields();
          getHospitalList();
        }}>清空</Button>
      </Form.Item>
    </Form>

    <div className={styles.mb}>
        <Button type="primary" className={styles.mr} onClick={()=>{navigate("/syt/hospital/hospitalSet/add")}}>添加</Button>
        <Button type="primary" danger disabled={!idList.length} onClick={batchDelete}>批量删除</Button>
    </div>

    <Table 
      rowSelection={rowSelection} //复选框
      dataSource={hospitalList} //数据
      columns={columns} //数据表头
      bordered //表格边框
      scroll={{x:1500}} //用于固定列的数据宽度
      rowKey={(row)=>row.id}//每行数据的key
      loading={isLoading}//isLoading状态
      pagination={{//分页器
        total:total,//数据条数
        current:current,//当前页码
        pageSize:pageSize,//每页显示条数
        showSizeChanger:true,//是否展示pageSize选择器
        pageSizeOptions:[3,5,10,20,50,100],//pageSize选择器选项,
        onChange:(currentPage:number,curpageSize:number):void=>{
          setCurrent(currentPage);
          setpageSize(curpageSize);
        },//分页切换事件
        // onShowSizeChange:(_:number,choosePageSize:number):void=>{
        //   setpageSize(choosePageSize);
        // }//pageSzie选项卡切换事件
      }}
    />
    </Card>
  )
}
