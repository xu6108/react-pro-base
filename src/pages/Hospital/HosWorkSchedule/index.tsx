import React, { useEffect, useState } from 'react'
import { Button, Card, Col, Pagination, Row, Table, Tag, Tree, TreeProps } from 'antd';
import { DataNode } from 'antd/es/tree';
import { useLocation, useNavigate } from 'react-router-dom'
import {reqAdministrativeOfficeByCode,type hosScheduleListType,reqFindScheduleList,type reqExpFindScheduleListType,reqGetScheduleRule,type bookingScheduleListType,type bookingScheduleList } from '@/api/HospitalList';
import { Key } from 'rc-tree/lib/interface';

export default function HosWorkSchedule() {
    const navigate = useNavigate();//编程式路由导航
    const {state} = useLocation();//获取数据
    const hoscode = (state as any).hoscode;//获取医院列表传过来的医院编码
    const [hospitalDepartment,setHospitalDepartment] = useState<hosScheduleListType>();//树形结构数据
    const [current,setCurrent] = useState(1);//日期-分页-当前页
    const [pageSize,setPageSize] = useState(5);//日期-分页-页码
    const [total,setTotal] = useState(0);//日期-分页-总条数
    const [tagData,setTagData] = useState<bookingScheduleListType>([]);//每科科室的预约时间
    const [selectedTreeKey,setSelectedTreeKey] = useState<any>("");//每个科室的唯一ID
    const [dataSourceData,setdataSourceData] = useState<reqExpFindScheduleListType>([]);//点击日期-列表数据
    const [isloading,setisloading] = useState(false);//表格菊花图
    const [docIndex,setDocIndex] = useState(-1);//点击日期显示样式 docIndex是下标

    //接口请求->初始化树数据
    useEffect(()=>{
        const getHosDpmList = async()=>{
            if(!hoscode)return;
            const result  = await reqAdministrativeOfficeByCode(hoscode);
            setHospitalDepartment(result);
        }
        getHosDpmList();
    },[]);

    //依赖日期当前页、页码修改时调用日期tag标签数据
    useEffect(()=>{
        setDocIndex(-1);
        if(selectedTreeKey.length==0)return;
        getTagData(selectedTreeKey);
    },[current,pageSize])


    //接口请求->日期
    const getTagData=async(
        selectedKeys?: string | null
    )=>{
        const result = await reqGetScheduleRule({
            page:current,
            limit:pageSize,
            hoscode,
            depcode:selectedKeys as string
        });
        
        setTotal(result.total);//数据总条数
        setTagData(result.bookingScheduleList);//日期数据
        
        if(result.total < 1){//当前选择科室无数据时，将日期tag、列表数据清空
            setdataSourceData([]);
            setTagData([]);
            return ;
        }
     };

     //点击日期事件
    const tagBtn = (item: bookingScheduleList,index:number)=>{   
        return async()=>{
            setisloading(true);//菊花图加载
            const result = await reqFindScheduleList({//当前日期的表格预约数据
                hoscode,
                depcode:selectedTreeKey,
                workDate:item.workDate,
            });
            setdataSourceData(result);//日期表格数据
            setisloading(false);//菊花图停止加载
            setDocIndex(index);
        }
    }

    //点击每科科室触发的事件
    const onSelect: TreeProps['onSelect'] = async(selectedKeys, info) => {
       setSelectedTreeKey(selectedKeys[0]);//将科室编号设置成响应式数据
       getTagData(selectedKeys[0] as string);
    };

    //表头
    const columns=[
        {
            title:"序号",
            render(_:any,__:any,index:number){
                return index+1;
            }
        },{
            title:"职称",
            dataIndex:"title"
        },{
            title:"号源时间",
            dataIndex:"workDate"
        },{
            title:"可预约次数",
            dataIndex:"availableNumber"
        },{
            title:"剩余预约次数",
            dataIndex:"reservedNumber"
        },{
            title:"挂号费(元)",
            dataIndex:"amount"
        },{
            title:"擅长技能",
            dataIndex:"skill"
        },
    ]

     return (
        <Card>
            <Row gutter={20}>
                <Col span={5}>
                <Tree
                    expandedKeys={["a4e171f4cf9b6816acdfb9ae62c414d7"]}
                    defaultExpandedKeys={['0-0-0', '0-0-1']}
                    onSelect={onSelect}
                    treeData={hospitalDepartment as []}
                    fieldNames={{key:"depcode",title:"depname"}}
                />
                </Col>
                <Col span={19}>
                    <div>
                        {
                            tagData.map((item,index)=>{
                                return (<Tag key={index} onClick={tagBtn(item,index)} style={{background:docIndex===index?"pink":""}}>
                                        <p>{item.workDate}</p>
                                        <p>{item.availableNumber}/{item.reservedNumber}</p>
                                    </Tag>
                                )
                            })
                        }
                    </div>

                    <Pagination 
                        current={current}
                        pageSize={pageSize}
                        total={total}
                        pageSizeOptions={[3, 5, 7, 9]}
                        showSizeChanger
                        style={{ marginTop: "30px" }}
                        onChange={(current,pageSize)=>{
                            setCurrent(current);
                            setPageSize(pageSize);
                        }}
                    ></Pagination>

                    <Table
                        loading={isloading}
                        style={{ marginTop: "10px" }}
                        columns={columns}
                        dataSource={dataSourceData}
                        rowKey={row=>row.id}
                    ></Table>

                    <Button type='primary' onClick={()=>{
                        navigate("/syt/hospital/hospitalList");
                    }}>返回</Button>
                </Col>
            </Row>
        </Card>
    ) 

}   
