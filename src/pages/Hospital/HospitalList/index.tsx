import React, { useEffect, useState } from "react";
import { Button, Card, Form, Input, Select, Table,message } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import { type provinceListType, reqProvinceData, reqCityOrCounty, reqHosList, type hosListParamsType, type hosDataListType,type hosDataListItemType,reqUpdStatus, } from "@api/HospitalList"
import { useNavigate } from "react-router-dom";

export default function HospitalList() {
  const [form] = Form.useForm();
  const [messageApi,contentHolder] = message.useMessage();
  const navigate = useNavigate();
  const [total, setTotal] = useState(0);
  const [current, setCurrent] = useState(1);
  const [pageSize, setPageSize] = useState(3);
  const [provinceList, setprovinceList] = useState<provinceListType>([]);
  const [cityList, setcityList] = useState<provinceListType>([]);
  const [countyList, setcountyList] = useState<provinceListType>([]);
  const [hospitalList,sethospitalList] = useState<hosDataListType>([]);
  const [isLoading,setisLoading] = useState(false);
  const [hosTypeList,sethosTypeList] = useState<provinceListType>([]);
  
  //省份
  useEffect(() => {
    const getProvinceList = async () => {
      const result = await reqProvinceData();
      setprovinceList(result);
    }
    getProvinceList();
  }, [])

  //初始化列表数据
  useEffect(()=>{
    getreqHosList();
  },[current,pageSize])

  //初始化请求医院类型
  useEffect(()=>{
    const gethosTypeList = async()=>{
      const result = await reqCityOrCounty("10000");
      sethosTypeList(result);
    }
    gethosTypeList();
  },[]);

  //医院列表接口查询、列表渲染
  const getreqHosList = async()=>{
    setisLoading(true);
    const {hoscode,
      hosname,
      hostype,
      provinceCode,
      cityCode,
      districtCode,
      status} = form.getFieldsValue(true);

    const result = await reqHosList({
      page:current,
      limit:pageSize,
      hoscode,
      hosname,
      hostype,
      provinceCode,
      cityCode,
      districtCode,
      status,
    });
    sethospitalList(result.content);
    setTotal(result.totalElements);
    setisLoading(false);
  }

  //查询事件
  const finish = async (values: hosListParamsType) => {
    getreqHosList()
  }

  const columns = [
    {
      title: "序号",
      render(_: any, __: any, index: number) {
        return index + 1;
      },
    },
    {
      title: "医院logo",
      dataIndex: "logoData",
      render(row: any) {
        return (
          <img
            src={"data:image/jpge;base64," + row}
            style={{ width: "100px" }}
          />
        );
      },
    },
    {
      title: "医院名称",
      dataIndex: "hosname",
    },
    {
      title: "等级",
      dataIndex: "param",
      render(param: any) {
        return param.hostypeString;
      },
    },
    {
      title: "详细地址",
      dataIndex: "param",
      render(param: any) {
        return param.fullAddress;
      },
    },
    {
      title: "状态",
      dataIndex: "status",
      render(status: any) {
        return status ? "上线" : "下线";
      },
    },
    {
      title: "创建时间",
      dataIndex: "createTime",
    },
    {
      title: "操作",
      width: "270px",
      render(row:hosDataListItemType) {
        return (
          <div>
            <Button type="primary" style={{ marginRight: "10px" }}
              onClick={()=>{
                navigate("/syt/hospital/hospitalList/hospitalDetail",{
                  state:{
                    id:row.id
                  }
                })
            }}>查看
            </Button>
            <Button type="primary" style={{ marginRight: "10px" }} onClick={()=>{
              navigate(`/syt/hospital/hospitalList/hosWorkSchedule`,{
                state:{
                  hoscode:row.hoscode
                }
              })
            }}>
              排班
            </Button>
            <Button type="primary" onClick={
              async()=>{
              const state={
                id:row.id,
                status:(row.status===0?1:0)
              }
              await reqUpdStatus(state);
              messageApi.open({
                type:"success",
                content:"操作成功"
              })
              getreqHosList();
            }}>{row.status===0?"上线":"下线"}</Button>
          </div>
        );
      },
    },
  ];

  return (
    <Card>
      {contentHolder}
      <Form layout="inline" form={form} onFinish={finish}>
        <Form.Item name="provinceCode">
          <Select
            placeholder="请选择省份"
            style={{ width: "200px", marginBottom: "20px" }}
            allowClear
            onChange={async (value) => {
              form.setFieldsValue({cityCode: undefined,districtCode: undefined});
              const result = await reqCityOrCounty(value);
              setcityList(result);
            }}>
            {
              provinceList.map(item => {
                return (
                  <Select.Option value={item.value} key={item.value}>{item.name}</Select.Option>
                )
              })
            }
          </Select>
        </Form.Item>
        <Form.Item name="cityCode">
          <Select
            placeholder="请选择城市"
            style={{ width: "200px", marginBottom: "20px" }}
            allowClear
            onChange={async (value) => {
              form.setFieldsValue({districtCode: undefined});
              const result = await reqCityOrCounty(value);
              setcountyList(result);
            }}>
            {
              cityList.map(item => {
                return (
                  <Select.Option value={item.value} key={item.value}>{item.name}</Select.Option>
                )
              })
            }
          </Select>
        </Form.Item>
        <Form.Item name="districtCode">
          <Select
            placeholder="请选择区县"
            allowClear
            style={{ width: "200px", marginBottom: "20px" }} >
            {
              countyList.map(item => {
                return (
                  <Select.Option value={item.value} key={item.value}>{item.name}</Select.Option>
                )
              })
            }
          </Select>
        </Form.Item>
        <Form.Item name="hosname">
          <Input
            placeholder="医院名称"
            allowClear
            style={{ width: "200px", marginBottom: "20px" }}
          />
        </Form.Item>
        <Form.Item name="hoscode">
          <Input
            placeholder="医院编号"
            allowClear
            style={{ width: "200px", marginBottom: "20px" }}
          />
        </Form.Item>
        <Form.Item name="hostype">
          <Select
            placeholder="医院类型"
            allowClear
            style={{ width: "200px", marginBottom: "20px" }}
          >
            {
              hosTypeList.map(item=>{
                return (
                  <Select.Option value={item.value} key={item.value}>{item.name}</Select.Option>
                )
              })
            }
          </Select>
        </Form.Item>
        <Form.Item name="status">
          <Select
            placeholder="医院状态"
            allowClear
            style={{ width: "200px", marginBottom: "20px" }}
          >
            <Select.Option value={1}>上线</Select.Option>
            <Select.Option value={0}>下线</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item>
          <Button
            icon={<SearchOutlined />}
            type="primary"
            htmlType="submit"
            style={{ marginRight: "20px" }}
          >
            查询
          </Button>
          <Button onClick={() => {
            form.resetFields();
            getreqHosList();
          }}>清空</Button>
        </Form.Item>
      </Form>
      <Table
        loading={isLoading}
        columns={columns}
        dataSource={hospitalList}
        rowKey={(row) => row.id}
        bordered
        pagination={{
          //数据总数
          total: total,
          //当前页码
          current: current,
          //每页条数
          pageSize: pageSize,
          //是否展示pageSize选择器
          showSizeChanger: true,
          //pageSize选择器的选项
          pageSizeOptions: [3, 5, 7, 10],
          onChange(current:number,pageSize:number){
            setCurrent(current);
            setPageSize(pageSize);
          }
        }}
      ></Table>
    </Card>
  );
}
