import { requestHos } from "@/utils/http";

//医院列表get 形参ts类型
export interface hosReqStateType {
  page: number;
  limit: number;
  hosname?: string;
  hoscode?: string;
}

//数据中的每一项设置类型
export interface hosReqListItemType {
  "id": number,
  "createTime": string,
  "updateTime": string,
  "isDeleted": number,
  "param": any,
  "hosname": string,
  "hoscode": string,
  "apiUrl": string,
  "signKey": string,
  "contactsName": string,
  "contactsPhone": string,
  "status": number
}

//给数据的每一项设置别名类型
export type hosReqDataType = hosReqListItemType[];

//axios返回的是promise对象 
//要限制promise对象返回值类型
export interface hosReqAllDataType {
  "records": hosReqDataType,
  "total": number,
  "size": number,
  "current": number,
  "orders": any[],
  "hitCount": boolean,
  "searchCount": boolean,
  "pages": number
}

export interface reqHospitalAddState{
  "id"?:number,
  "apiUrl": string,
  "contactsName": string,
  "contactsPhone": string,
  "hoscode": string,
  "hosname": string
}

// 请求医院列表
// axios请求的形参和返回值必须要设置类型 //
export const reqHospital = ({page=1,limit=3,hosname="",hoscode=""}:hosReqStateType) => {
  // <any, string>的第一个类型为any即可，实际不会用上
  // 第二个类型是返回值数据中data的类型，根据接口文档填写
  return requestHos.get<any,hosReqAllDataType>(`/admin/hosp/hospitalSet/${page}/${limit}`, {
    params:{
      hosname,
      hoscode
    }
  });
};

//添加医院接口
export const reqHospitalAdd =(data:reqHospitalAddState)=>{

  return requestHos.post<any,null>("admin/hosp/hospitalSet/save",data);
}


//根据id获取医院数据接口
export const reqHospitalDataById =(id:number)=>{
  return requestHos.get<any,hosReqListItemType>(`/admin/hosp/hospitalSet/get/${id}`);
}


//修改医院数据接口
export const reqHospitalUpdData =(data:reqHospitalAddState)=>{
  return requestHos.put<any,hosReqListItemType>(`/admin/hosp/hospitalSet/update`,data);
}

//删除医院数据接口
export const reqHospitalDeleteByid =(id:number)=>{
  return requestHos.delete<any,null>(`/admin/hosp/hospitalSet/remove/${id}`);
}

//批量删除医院数据接口
export const reqHospitalBatchDeleteByidList =(idList:number[])=>{
  return requestHos.delete<any,null>(`/admin/hosp/hospitalSet/batchRemove`,{
    data:idList
  });
}