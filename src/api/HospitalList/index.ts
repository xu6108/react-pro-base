import { requestPro,requestHos } from "@/utils/http";


export interface provinceItemType {
    id: number;
    createTime: string;
    updateTime: string;
    isDeleted: number;
    param: object;
    parentId: number;
    name: string;
    value: string;
    dictCode: null;
    hasChildren: boolean;
}

export type provinceListType = provinceItemType[];


//3.请求医院列表params参数类型
export interface hosListParamsType {
  page: number;
  limit: number;
  hoscode?: string;
  hosname?: string;
  hostype?: string;
  provinceCode?: string;
  cityCode?: string;
  districtCode?: string;
  status?: number;
}

//医院列表数据返回值中bookingRule类型
export interface hosDataBookingRuleType {
  cycle: number;
  releaseTime: string;
  stopTime: string;
  quitDay: number;
  quitTime: string;
  rule: string[];
}
//医院列表数据返回值中param类型
export interface hosDataParamType {
  hostypeString: string;
  fullAddress: string;
}

//医院列表中每一项的类型
export interface hosDataListItemType {
  id: string;
  createTime: string;
  updateTime: string;
  isDeleted: number;
  param: hosDataParamType;
  hoscode: string;
  hosname: string;
  hostype: string;
  provinceCode: string;
  cityCode: string;
  districtCode: string;
  address: string;
  logoData: string;
  intro: string;
  route: string;
  status: number;
  bookingRule: hosDataBookingRuleType;
}

//医院列表的类型
export type hosDataListType = hosDataListItemType[];

//请求医院列表的返回值类型
export interface HosListDataType {
  content: hosDataListType;
  //以下这个类型用不到，我们不用分开写
  pageable: {
    sort: {
      sorted: boolean;
      unsorted: boolean;
      empty: boolean;
    };
    pageNumber: number;
    pageSize: number;
    offset: number;
    paged: boolean;
    unpaged: boolean;
  };
  last: boolean;
  //总页数
  totalPages: number;
  //数据总条数
  totalElements: number;
  first: boolean;
  //以下这个类型用不到，我们不用分开写
  sort: {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
  };
  numberOfElements: number;
  size: number;
  number: number;
  empty: boolean;
}

export interface reqHosListByIdType{
  "bookingRule": hosDataBookingRuleType
  "hospital": hosDataListItemType
}

//医院科室的每一项类型
export interface hosScheduleItemType {
  depcode: string;
  depname: string;
  children: hosScheduleListType | null;
}

//医院科室的列表类型
export type hosScheduleListType = hosScheduleItemType[];


//省份
export const reqProvinceData =()=>{
    return requestPro.get<any,provinceListType>(`/admin/cmn/dict/findByDictCode/province`);
}

export const reqCityOrCounty = (parentId: string) => {
    return requestPro.get<any, provinceListType>(
      `/admin/cmn/dict/findByParentId/${parentId}`
    );
};

export const reqHosList = (params: hosListParamsType) => {
  const {
    page,
    limit,
    hoscode = "",
    hosname = "",
    hostype = "",
    provinceCode = "",
    cityCode = "",
    districtCode = "",
    status,
  } = params;
  return requestHos.get<any, HosListDataType>(
    `/admin/hosp/hospital/${page}/${limit}`,
    {
      params: {
        hoscode,
        hosname,
        hostype,
        provinceCode,
        cityCode,
        districtCode,
        status,
      },
    }
  );
};

export const reqHosListById = (id:string)=>{ 
  return requestHos.get<any,reqHosListByIdType>(`/admin/hosp/hospital/show/${id}`);
}

export interface reqUpdStatusStateType{
  id:string,
  status:number
}

export const reqUpdStatus = ({id,status}:reqUpdStatusStateType )=>{ 
  return requestHos.get<any,null>(`admin/hosp/hospital/updateStatus/${id}/${status}`);
}

//根据医院编号查询数据
export const reqAdministrativeOfficeByCode = (hoscode:string)=>{ 
  return requestHos.get<any,hosScheduleListType>(`admin/hosp/department/${hoscode}`);
}

export type findScheduleListStateType={
  hoscode:string,
  depcode:string,
  workDate:string,
}


interface reqFindScheduleParamListType{
  "dayOfWeek": string,
  "depname": string,
  "hosname": string
}

export interface reqFindScheduleListType {
    "id": string,
    "createTime": string,
    "updateTime": string,
    "isDeleted": number,
    "param": reqFindScheduleParamListType,
    "hoscode": string,
    "depcode": string,
    "title": string,
    "docname": string,
    "skill": string,
    "workDate": string,
    "workTime": number,
    "reservedNumber": number,
    "availableNumber": number,
    "amount": number,
    "status": number,
    "hosScheduleId": string
}

export type reqExpFindScheduleListType = reqFindScheduleListType[];

//查询当前日期每位医生的预约次数
export const reqFindScheduleList = ({hoscode,depcode,workDate}:findScheduleListStateType)=>{
  return requestHos.get<any,reqExpFindScheduleListType>(`admin/hosp/schedule/findScheduleList/${hoscode}/${depcode}/${workDate}`);
}

export type reqGetScheduleRuleStateType={
  hoscode:string,
  depcode:string,
  page:number,
  limit:number
}

export interface bookingScheduleList{
  "workDate": string,
  "workDateMd": null,
  "dayOfWeek": string,
  "docCount": number,
  "reservedNumber": number,
  "availableNumber": number,
  "status": null
}

export type bookingScheduleListType = bookingScheduleList[];

export interface reqGetScheduleRuleType {
  "total": number,
  "bookingScheduleList": bookingScheduleListType,
  "baseMap": {
    "hosname": string
  }
}

//查询科室每日的预约量等信息
export const reqGetScheduleRule = ({hoscode,depcode,page,limit}:reqGetScheduleRuleStateType)=>{
  return requestHos.get<any,reqGetScheduleRuleType>(`/admin/hosp/schedule/getScheduleRule/${page}/${limit}/${hoscode}/${depcode}`);
}