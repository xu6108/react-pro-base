// src/routes/index.tsx
import React, { lazy, Suspense, FC } from "react";
import { useRoutes, Navigate } from "react-router-dom";
import { HomeOutlined,ReconciliationOutlined } from "@ant-design/icons";
import type { XRoutes } from "./types";

import {
  Layout,
  EmptyLayout,
  // CompLayout
} from "../layouts";
import Loading from "@comps/Loading";

import Translation from "@comps/Translation";

const Login = lazy(() => import("@pages/login"));
const Dashboard = lazy(() => import("@pages/dashboard"));
const NotFound = lazy(() => import("@pages/404"));
const HospitalList = lazy(()=>import("@pages/Hospital/HospitalList"));
const HospitalSet = lazy(()=>import("@pages/Hospital/HospitalSet"));
const HospitalAddOrUpdate = lazy(()=>import("@pages/Hospital/HospitalAddOrUpdate"));
const HospitalDetial = lazy(()=>import("@pages/Hospital/HsopitalDetail"))
const HosWorkSchedule = lazy(()=>import("@pages/Hospital/HosWorkSchedule"))

const load = (Comp: FC) => {
  return (
    // 因为路由懒加载，组件需要一段网络请求时间才能加载并渲染
    // 在组件还未渲染时，fallback就生效，来渲染一个加载进度条效果
    // 当组件渲染完成时，fallback就失效了
    <Suspense fallback={<Loading />}>
      {/* 所有lazy的组件必须包裹Suspense组件，才能实现功能 */}
      <Comp />
    </Suspense>
  );
};

const routes: XRoutes = [
  {
    path: "/",
    element: <EmptyLayout />,
    children: [
      {
        path: "login",
        element: load(Login),
      },
    ],
  },
  {
    path: "/syt",
    element: <Layout />,
    children: [
      {
        path: "/syt/dashboard",
        meta: {
          icon: <HomeOutlined />,
          title: <Translation>route:dashboard</Translation>,
        },
        element: load(Dashboard),
      },
      {
        path:"/syt/hospital",
        meta:{
          icon:<ReconciliationOutlined />,
          title:"医院管理",
        },
        children:[
          {
            path:"/syt/hospital/hospitalList",
            meta:{
              title:"医院列表"
            },
            element:load(HospitalList)
          },
          {
            path:"/syt/hospital/hospitalSet",
            meta:{
              title:"医院设置"
            },
            element:load(HospitalSet)
          },
          {
            path:"/syt/hospital/hospitalSet/add",
            meta:{
              title:"新增医院"
            },
            element:load(HospitalAddOrUpdate),
            hidden:true
          },
          {
            path:"/syt/hospital/hospitalSet/update/:id",
            meta:{
              title:"修改医院"
            },
            element:load(HospitalAddOrUpdate),
            hidden:true
          },
          {
            path:"/syt/hospital/hospitalList/hospitalDetail",
            meta:{
              title:"查询医院详情"
            },
            element:load(HospitalDetial),
            hidden:true
          },
          {
            path:"/syt/hospital/hospitalList/hosWorkSchedule",
            meta:{
              title:"医院排版"
            },
            element:load(HosWorkSchedule),
            hidden:true
          }
        ]
      }
    ],
  },

  {
    path: "/404",
    element: load(NotFound),
  },
  {
    path: "*",
    element: <Navigate to="/404" />,
  },
];

// 渲染路由
// 注意：首字母必须大写
export const RenderRoutes = () => {
  // react-router-dom的新增语法。不用自己遍历了，它帮我们遍历生成
  return useRoutes(routes);
};

// 找到要渲染成左侧菜单的路由
export const findSideBarRoutes = () => {
  const currentIndex = routes.findIndex((route) => route.path === "/syt");
  return routes[currentIndex].children as XRoutes;
};

export default routes;
