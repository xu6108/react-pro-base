import React from "react";
import { ConfigProvider } from "antd";
import zhCN from "antd/lib/locale/zh_CN";
import enUS from "antd/lib/locale/en_US";

import { useAppSelector } from "@/app/hooks";
import { selectLang } from "@/app/slice";

import { RenderRoutes } from "./routes";

function App() {
  //自定义hook
  const lang = useAppSelector(selectLang);

  //ConfigProvider antd提供的第三方组件
  return <ConfigProvider locale={lang === "zh_CN" ? zhCN : enUS}>{RenderRoutes()}</ConfigProvider>;
}

export default App;
